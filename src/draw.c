#include "draw.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include "stb_image.h"
#include "draw.h"
#include "quadtree.h"
#include "scene.h"

//Dessine le cube de la Skybox
void drawBox(float length, GLuint* textureSky) {
    glEnable(GL_TEXTURE_2D);
    
    float l = length/2;
    //glColor3f(1., 1., 1.);
    glColor3f(120, 120, 120);

   /***** 1 = face 1 ******/
    glBindTexture(GL_TEXTURE_2D, textureSky[0]);
    glBegin(GL_QUADS);
        glTexCoord2f(0.,1.); glVertex3f(-l , -l, l);
        glTexCoord2f(0.,0.); glVertex3f(-l , -l, -l);
        glTexCoord2f(1.,0.); glVertex3f(-l , l, -l);
        glTexCoord2f(1.,1.); glVertex3f(-l , l, l);
    glEnd();

   /***** 2 = TOP NOOOOOO ******/
    glBindTexture(GL_TEXTURE_2D, textureSky[1]);
    glBegin(GL_QUADS);
        glTexCoord2f(1.,1.); glVertex3f(-l , l, -l);
        glTexCoord2f(1.,0.); glVertex3f(-l , l, l);
        glTexCoord2f(0.,0.); glVertex3f(l , l, l);
        glTexCoord2f(0.,1.); glVertex3f(l , l, -l);
    glEnd();

   /***** 3 = face 2 ******/
glBindTexture(GL_TEXTURE_2D, textureSky[2]);
    glBegin(GL_QUADS);
        glTexCoord2f(1.,1.); glVertex3f(l , -l, -l);
        glTexCoord2f(1.,0.); glVertex3f(l , -l, l);
        glTexCoord2f(0.,0.); glVertex3f(l , l, l);
        glTexCoord2f(0.,1.); glVertex3f(l , l, -l);
    glEnd();


   /***** 4 = FLOOR NOOOOO ******/
    glBindTexture(GL_TEXTURE_2D, textureSky[3]);
    glBegin(GL_QUADS);
        glTexCoord2f(1.,1.); glVertex3f(l , -l, -l);
        glTexCoord2f(1.,0.); glVertex3f(l , -l, l);
        glTexCoord2f(0.,0.); glVertex3f(-l , -l, l);
        glTexCoord2f(0.,1.);glVertex3f(-l , -l, -l);
    glEnd();


   /***** 5 = face 3 ******/
    glBindTexture(GL_TEXTURE_2D, textureSky[4]);
    glBegin(GL_QUADS);
        glTexCoord2f(0.,0.); glVertex3f(-l , -l, -l);
        glTexCoord2f(1.,0.); glVertex3f(-l , l, -l);
        glTexCoord2f(1.,1.); glVertex3f(l , l, -l);
        glTexCoord2f(0.,1.); glVertex3f(l , -l, -l);
    glEnd();


   /***** 6 = face 4 ******/
    glBindTexture(GL_TEXTURE_2D, textureSky[5]);
    glBegin(GL_QUADS);
        glTexCoord2f(0.,0.); glVertex3f(-l , -l, l);
        glTexCoord2f(1.,0.); glVertex3f(-l , l, l);
        glTexCoord2f(1.,1.); glVertex3f(l , l, l);
        glTexCoord2f(0.,1.); glVertex3f(l , -l, l);
    glEnd();
    

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);   
    glEnd();
}



//récupérer les textures de la skybox
GLuint getJpgTextureJpg(char* path){
    GLuint Nbtexture;
    
    SDL_Surface* image = IMG_Load(path);
    if(!image){
        printf("%s\n", IMG_GetError());
    }
    else{
    glGenTextures(1, &Nbtexture);
    glBindTexture(GL_TEXTURE_2D, Nbtexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
     }
    return Nbtexture;
}

GLuint getPngTexturePng(char* path){
    GLuint Nbtexture;
    
    SDL_Surface* image = IMG_Load(path);
    if(!image){
        printf("%s\n", IMG_GetError());
    }
    else{    
    glGenTextures(1,&Nbtexture);
    glBindTexture(GL_TEXTURE_2D, Nbtexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    SDL_FreeSurface(image);
     }
    return Nbtexture;
}


//changer le mode de visualisation
int toggleModeVisu(int* modeVisu){
    if(*modeVisu == 0){
        return 1;
    }
    else{
        return 0;
    }
}