#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "heightmap.h"
#include "quadtree.h"

void SkipComments(FILE *fp)
{
    int ch;
    char line[100];
    while ((ch = fgetc(fp)) != EOF && isspace(ch)) {
        ;
    }
 
    if (ch == '#') {
        fgets(line, sizeof(line), fp);
        SkipComments(fp);
    } else {
        fseek(fp, -1, SEEK_CUR);
    }
} 


void loadPGM(const char* file_path, PGMData* data)
{
    FILE *pgmFile;
    pgmFile = fopen(file_path, "rb");

    if (pgmFile == NULL) {
        perror("cannot open file to read");
        exit(EXIT_FAILURE);
    }

    char versionPGM[3];
    fgets(versionPGM, sizeof(versionPGM), pgmFile);

    if (strcmp(versionPGM, "P2") != 0) {
        fprintf(stderr, "Wrong file type!\n");
        exit(EXIT_FAILURE);
    }

    SkipComments(pgmFile);
    fscanf(pgmFile, "%d", &data->width);
    SkipComments(pgmFile);
    fscanf(pgmFile, "%d", &data->height);
    SkipComments(pgmFile);
    fscanf(pgmFile, "%d", &data->max_gray);
    fgetc(pgmFile);
    //printf("PGM DATA: %s %d %d %d\n", versionPGM, data->width, data->height, data->max_gray);


    int i, j;

    //ALLOCATION MEMOIRE POUR PIXELS
	data->pixels = (int **)malloc(sizeof(int *) * data->width);
    if (data->pixels == NULL) {
        perror("memory allocation failure");
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < data->width; ++i) {
        data->pixels[i] = (int *)malloc(sizeof(int) * data->height);
        if (data->pixels[i] == NULL) {
            perror("memory allocation failure");
            exit(EXIT_FAILURE);
        }
    }

    //REMPLIR LA MATRICE DE PIXELS
    for (i = 0; i < data->width; ++i) {
        for (j = 0; j < data->height; ++j) {
            fscanf(pgmFile, "%d", &data->pixels[i][j]);
        }
    }

    fclose(pgmFile);

    //return data;
}


Heightmap* initializeHeightmap(const char* infos_path, Heightmap* heightmap){

    FILE* timacFile;
    timacFile = fopen(infos_path, "rb");
    if (timacFile == NULL) {
        perror("cannot open file timac to read");
        exit(EXIT_FAILURE);
    }

    char* pgmName[50];
    fscanf(timacFile, "%s %d %d %f %f", pgmName, &heightmap->xsize, &heightmap->ysize, &heightmap->zmin, &heightmap->zmax);

    //TEST PGM
	PGMData* PGMHeightmap = (PGMData*) malloc(sizeof(PGMData));
	//PGMData* PGMHeightmap;
	const char* pgm_path = pgmName;
	loadPGM(pgm_path, PGMHeightmap);
	//printf("PGMHeightmap IN INITIALIZE HEIGHTMAP: %d %d %d\n", PGMHeightmap->width, PGMHeightmap->height, PGMHeightmap->max_gray);

    heightmap->pmginfos = PGMHeightmap;

    //printf("HEIGHTMAP TIMAC: %d %d %d %f %f \n", heightmap->pmginfos->max_gray, heightmap->xsize, heightmap->ysize, heightmap->zmin, heightmap->zmax);

    heightmap->quadtree=InitializeQuadtree(heightmap);
    insertNodes(heightmap->quadtree, heightmap);

    return heightmap;
};