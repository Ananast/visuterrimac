#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "quadtree.h"
#include "geometry.h"

Node* InitializeQuadtree(Heightmap* heightmap){
    Node* nodeInitial = (Node*) malloc(sizeof(Node));

    nodeInitial->HautGauche = NULL;
    nodeInitial->HautDroite = NULL;
    nodeInitial->BasGauche = NULL;
    nodeInitial->BasDroite = NULL;

    int x,y,z;

    x=0;
    y=0;
    z=heightmap->pmginfos->pixels[x][y];
    nodeInitial->limitHG = createPoint3D(x, y, z);

    x=heightmap->pmginfos->width-1;
    y=heightmap->pmginfos->height-1;
    z=heightmap->pmginfos->pixels[x][y];
    //printf("INFOS HEIGHTMAP BD DANS : %d %d %d \n", x, y, z);
    nodeInitial->limitBD = createPoint3D(x, y, z);

    nodeInitial->leaf = NULL;

    return nodeInitial;
}


Point3D* createPoint3D(int x, int y, int z){
    Point3D* newPoint3D = (Point3D*) malloc(sizeof(Point3D));
    newPoint3D->x = x;
    newPoint3D->y = y;
    newPoint3D->z = z;

    return newPoint3D;
}


void insertNodes(Node* node, Heightmap* heightmap){
    int espacewidth = abs(node->limitBD->x - node->limitHG->x);
    int espaceheight = abs(node->limitBD->y - node->limitHG->y);

    if(espacewidth>1){

        //SUB NODE HAUT GAUCHE
        //************************************************
        node->HautGauche = (Node*) malloc(sizeof(Node));
        node->HautGauche->limitHG = node->limitHG;

        //POUR LE POINT EN BAS A DROITE
        int x,y,z;
        x=node->limitHG->x + espacewidth/2;
        y=node->limitHG->y + espaceheight/2;
        z=heightmap->pmginfos->pixels[x][y];

        node->HautGauche->limitBD = createPoint3D(x, y, z);


        /*printf(" add POINT HG pour subNode HAUT GAUCHE : %d, %d, %d\n", node->HautGauche->limitHG->x, node->HautGauche->limitHG->y, node->HautGauche->limitHG->z);
        printf(" add POINT BD pour subNode HAUT GAUCHE : %d, %d, %d\n", node->HautGauche->limitBD->x, node->HautGauche->limitBD->y, node->HautGauche->limitBD->z);
        printf("----------------\n");*/

        node->HautGauche->leaf = NULL;

        insertNodes(node->HautGauche, heightmap);
        //************************************************


        
        //SUB NODE HAUT DROITE
        //************************************************
        node->HautDroite = (Node*) malloc(sizeof(Node));

        //POUR LE POINT EN HAUT A GAUCHE
        x=node->limitHG->x + espacewidth/2;
        y=node->limitHG->y;
        z=heightmap->pmginfos->pixels[x][y];
        node->HautDroite->limitHG = createPoint3D(x, y, z);


        //POUR LE POINT EN BAS A DROITE
        x=node->limitHG->x+espacewidth;
        y=node->limitHG->y + espaceheight/2;
        z=heightmap->pmginfos->pixels[x][y];
        node->HautDroite->limitBD = createPoint3D(x, y, z);


        /*printf(" POINT HG pour subNode HAUT Droite : %d, %d, %d\n", node->HautDroite->limitHG->x, node->HautDroite->limitHG->y, node->HautDroite->limitHG->z);
        printf(" add POINT BD pour subNode HAUT Droite : %d, %d, %d\n", node->HautDroite->limitBD->x, node->HautDroite->limitBD->y, node->HautDroite->limitBD->z);
        printf("----------------\n");*/

        node->HautDroite->leaf = NULL;

        insertNodes(node->HautDroite, heightmap);
        //************************************************



        //SUB NODE BAS GAUCHE
        //************************************************
        node->BasGauche = (Node*) malloc(sizeof(Node));

        //POUR LE POINT EN HAUT A GAUCHE
        x=node->limitHG->x;
        y=node->limitHG->y + espaceheight/2;
        z=heightmap->pmginfos->pixels[x][y];
        node->BasGauche->limitHG = createPoint3D(x, y, z);


        //POUR LE POINT EN BAS A DROITE
        x=node->limitHG->x + espaceheight/2;
        y=node->limitHG->y+espaceheight;
        z=heightmap->pmginfos->pixels[x][y];
        node->BasGauche->limitBD = createPoint3D(x, y, z);


        /*printf(" add POINT HG pour subNode BAS GAUCHE : %d, %d, %d\n", node->BasGauche->limitHG->x, node->BasGauche->limitHG->y, node->BasGauche->limitHG->z);
        printf(" add POINT BD pour subNode BAS GAUCHE : %d, %d, %d\n", node->BasGauche->limitBD->x, node->BasGauche->limitBD->y, node->BasGauche->limitBD->z);
        printf("----------------\n");*/

        node->BasGauche->leaf = NULL;

        insertNodes(node->BasGauche, heightmap);
        //************************************************


        //SUB NODE BAS DROITE
        //************************************************
        node->BasDroite = (Node*) malloc(sizeof(Node));

        //POUR LE POINT EN HAUT A GAUCHE
        x=node->limitHG->x + espacewidth/2;
        y=node->limitHG->y + espaceheight/2;
        z=heightmap->pmginfos->pixels[x][y];
        node->BasDroite->limitHG = createPoint3D(x, y, z);


        //POUR LE POINT EN BAS A DROITE
        x=node->limitBD->x;
        y=node->limitBD->y;
        z=heightmap->pmginfos->pixels[x][y];
        node->BasDroite->limitBD = createPoint3D(x, y, z);

        /*printf(" add POINT BD pour subNode BAS Droite : %d, %d, %d\n", node->BasDroite->limitHG->x, node->BasDroite->limitHG->y, node->BasDroite->limitHG->z);
        printf(" add POINT BD pour subNode BAS Droite : %d, %d, %d\n", node->BasDroite->limitBD->x, node->BasDroite->limitBD->y, node->BasDroite->limitBD->z);
        printf("----------------\n");*/

        node->BasDroite->leaf = NULL;

        insertNodes(node->BasDroite, heightmap);
        //************************************************
        
    }
    //on cree une leaf
    else{

        node->HautGauche = NULL;
        node->HautDroite = NULL;
        node->BasGauche = NULL;
        node->BasDroite = NULL;

        node->leaf = createLeaf(node->limitHG, node->limitBD, heightmap);

        //printf("     ON CREE UNE LEAF QUAND ON CREE LES NODES \n");
    }

}


Leaf* createLeaf(Point3D* limitHG, Point3D* limitBD, Heightmap* heightmap){
    Leaf* newLeaf = (Leaf*) malloc(sizeof(Leaf));

    int x,y,z;

    newLeaf->pointHG = limitHG;

    x=limitBD->x;
    y=limitHG->y;
    z=heightmap->pmginfos->pixels[x][y];
    newLeaf->pointHD = createPoint3D(x,y,z);

    x=limitHG->x;
    y=limitBD->y;
    z=heightmap->pmginfos->pixels[x][y];
    newLeaf->pointBG = createPoint3D(x,y,z);

    newLeaf->pointBD = limitBD;

    return newLeaf;
}


void drawLeaves(Node* node, Heightmap* heightmap, GLuint* tabTextureMap, int* modeVisu){
    if(node->leaf == NULL){

        /* 
        --- FRUSTUM CULLING ---

        //si un des points du node suivant est visible, on continue de diviser l'espace

        if(frustumCullingPossible(node->HautGauche, heightmap, double eyeX, double eyeY, double eyeZ, int fov) == 0){
            drawLeaves(node->HautGauche, heightmap, tabTextureMap, modeVisu);
        }
        if(frustumCullingPossible(node->HautDroite, heightmap, double eyeX, double eyeY, double eyeZ, int fov) == 0){
            drawLeaves(node->HautDroite, heightmap, tabTextureMap, modeVisu);
        }
        if(frustumCullingPossible(node->BasGauche, heightmap, double eyeX, double eyeY, double eyeZ, int fov) == 0){
            drawLeaves(node->BasGauche, heightmap, tabTextureMap, modeVisu);
        }
        if(frustumCullingPossible(node->BasDroite, heightmap, double eyeX, double eyeY, double eyeZ, int fov) == 0){
            drawLeaves(node->BasDroite, heightmap, tabTextureMap, modeVisu);
        }

        */

        drawLeaves(node->HautGauche, heightmap, tabTextureMap, modeVisu);
        drawLeaves(node->HautDroite, heightmap, tabTextureMap, modeVisu);
        drawLeaves(node->BasGauche, heightmap, tabTextureMap, modeVisu);
        drawLeaves(node->BasDroite, heightmap, tabTextureMap, modeVisu);
    }
    else{
        //C'est une leaf, on la draw
        
        float widthPgm = (float)heightmap->pmginfos->width;
        float xsize = (float)heightmap->xsize;

        float heightPgm = (float)heightmap->pmginfos->height;
        float ysize = (float)heightmap->ysize;

        float hauteurPgm = (float)heightmap->pmginfos->max_gray;
        float zmin = (float)heightmap->zmin;
        float zmax = (float)heightmap->zmax-(float)heightmap->zmin;


        float moyennehauteur = (zmin+((float)node->leaf->pointHG->z/hauteurPgm)*(zmax-zmin) + zmin+((float)node->leaf->pointBG->z/hauteurPgm)*(zmax-zmin)) /2;
        int chooseTexture;
        if(moyennehauteur<(zmin+((zmax-zmin)/8))){
            chooseTexture = 0;
        }
        else if(moyennehauteur<(zmin+((zmax-zmin)/2))){
            chooseTexture = 1;
        }
        else{
            chooseTexture = 2;
        }

        if(*modeVisu == 0){
            //il faut draw en mode texture
            drawLeavesTexMode(node, tabTextureMap, widthPgm, xsize, heightPgm, ysize, hauteurPgm, zmin, zmax, moyennehauteur, chooseTexture);
        }
        else {
            //il faut draw en mode fil de fer quadtree
            drawLeavesLineMode(node, widthPgm, xsize, heightPgm, ysize, hauteurPgm, zmin, zmax, moyennehauteur);
        }
    }
}



void drawLeavesLineMode(Node* node, float widthPgm, float xsize, float heightPgm, float ysize, float hauteurPgm, float zmin, float zmax, float moyennehauteur){
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

    glBegin(GL_TRIANGLES);
            glColor3f(255.,255.,255.);   
                glVertex3f(((float)node->leaf->pointHG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHG->z/hauteurPgm)*zmax, ((float)node->leaf->pointHG->y/heightPgm)*ysize);
                glVertex3f(((float)node->leaf->pointHD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHD->z/hauteurPgm)*zmax, ((float)node->leaf->pointHD->y/heightPgm)*ysize);
                glVertex3f(((float)node->leaf->pointBG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBG->z/hauteurPgm)*zmax, ((float)node->leaf->pointBG->y/heightPgm)*ysize);
    glEnd();
    glBegin(GL_TRIANGLES);
            glColor3f(255.,255.,255.);
                glVertex3f(((float)node->leaf->pointBG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBG->z/hauteurPgm)*zmax, ((float)node->leaf->pointBG->y/heightPgm)*ysize);
                glVertex3f(((float)node->leaf->pointHD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHD->z/hauteurPgm)*zmax, ((float)node->leaf->pointHD->y/heightPgm)*ysize);
                glVertex3f(((float)node->leaf->pointBD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBD->z/hauteurPgm)*zmax, ((float)node->leaf->pointBD->y/heightPgm)*ysize);
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        
}

void drawLeavesTexMode(Node* node, GLuint* tabTextureMap, float widthPgm, float xsize, float heightPgm, float ysize, float hauteurPgm, float zmin, float zmax, float moyennehauteur, int chooseTexture){
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tabTextureMap[chooseTexture]);

    glBegin(GL_TRIANGLES);
            glColor3f(255.,255.,255.);
                glTexCoord2f(0.,0.);   
                glVertex3f(((float)node->leaf->pointHG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHG->z/hauteurPgm)*zmax, ((float)node->leaf->pointHG->y/heightPgm)*ysize);
                glTexCoord2f(0.,1.);
                glVertex3f(((float)node->leaf->pointHD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHD->z/hauteurPgm)*zmax, ((float)node->leaf->pointHD->y/heightPgm)*ysize);
                glTexCoord2f(1.,0.);
                glVertex3f(((float)node->leaf->pointBG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBG->z/hauteurPgm)*zmax, ((float)node->leaf->pointBG->y/heightPgm)*ysize);
        glEnd();
        //glBindTexture(GL_TEXTURE_2D, tabTextureMap[chooseTexture]);
        glBegin(GL_TRIANGLES);
            glColor3f(255.,255.,255.);
                glTexCoord2f(1.,0.);
                glVertex3f(((float)node->leaf->pointBG->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBG->z/hauteurPgm)*zmax, ((float)node->leaf->pointBG->y/heightPgm)*ysize);
                glTexCoord2f(0.,1.);
                glVertex3f(((float)node->leaf->pointHD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointHD->z/hauteurPgm)*zmax, ((float)node->leaf->pointHD->y/heightPgm)*ysize);
                glTexCoord2f(1.,1.);
                glVertex3f(((float)node->leaf->pointBD->x/widthPgm)*xsize, zmin+((float)node->leaf->pointBD->z/hauteurPgm)*zmax, ((float)node->leaf->pointBD->y/heightPgm)*ysize);
        glEnd();

        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);   
        glEnd();
}




Leaf* searchLeaf(Node* node, int x, int y){
    int espacewidth = node->limitBD->x - node->limitHG->x;
    int espaceheight = node->limitBD->y - node->limitHG->y;

    
    if(node->leaf == NULL){
        if(x<espacewidth/2){
            if(y<espaceheight/2){
                searchLeaf(node->HautGauche, x, y);
            }
            else{
                searchLeaf(node->BasGauche, x, y);
            }
        }
        else{
            if(y<espaceheight/2){
                searchLeaf(node->HautDroite, x, y);
            }
            else{
                searchLeaf(node->BasDroite, x, y);
            }
        }
    }
    else{    
        return node->leaf;
    }
}

int frustumCullingPossible(Node* node, Heightmap* heightmap, double eyeX, double eyeY, double eyeZ, int fov){
    //Point3D* camPos = (Point3D*) malloc(sizeof(Point3D));
    //eyeX et offX sont des float --> point3D prend des int;
    //camPos = createPoint3D((int)eyeX, (int)eyeY, (int)eyeZ);

    float widthPgm = (float)heightmap->pmginfos->width;
    float xsize = (float)heightmap->xsize;

    float heightPgm = (float)heightmap->pmginfos->height;
    float ysize = (float)heightmap->ysize;

    float hauteurPgm = (float)heightmap->pmginfos->max_gray;
    float zmin = (float)heightmap->zmin;
    float zmax = (float)heightmap->zmax-(float)heightmap->zmin;

    /* //attention ici ce sont points de leaf, remplacer par des nodes

    float xHG = ((float)node->leaf->pointHG->x/widthPgm)*xsize;
    float yHG = ((float)node->leaf->pointHG->y/heightPgm)*ysize;
    float zHG = zmin+((float)node->leaf->pointHG->z/hauteurPgm)*zmax;

    float xHD = ((float)node->leaf->pointHD->x/widthPgm)*xsize;
    float yHD = ((float)node->leaf->pointHD->y/heightPgm)*ysize;
    float zHD = zmin+((float)node->leaf->pointHD->z/hauteurPgm)*zmax;
    */

    //regarder si un des 4 points du node fait partie de l'espace visible par la camera. si oui, on return 0 et on doit continuer de drawleaves
    // return 0 si on peut pas s'arrêter, 1 si aucun des points ne fait partie du champ de vision et qu'on peut ne pas dessiner le node
    return 0;
}