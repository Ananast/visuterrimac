#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "heightmap.h"
#include "quadtree.h"
#include "scene.h"
#include "draw.h"
#include "camera.h"
#include "objects.h"
#include "geometry.h"

//Scene* sceneTest;
Heightmap* HeightmapTest;
Scene* scene;

// Textures pour le terrain
GLuint tabMapTextures[3];

// Texture pour les arbres
GLuint treeTexture;


/* variables globales pour la gestion de la caméra */
float profondeur = 3;
float latitude = 0.0;
float longitude = M_PI/2.;

static float teta = 0.;

GLuint tabTextureId[6];
GLuint tabTextureId2[1];
float obj_rot = 0.0;
unsigned int size_pt = 5;

//int pour changer le mode de visualisation de la map
int modeVisu = 0;

// montee descente des thwomps
static const float maxThwompMouv = 0.1, minThwompMouv = 0.;

float thwompMouvs[NB_THWOMP];
static int thwompsGetDowntab[NB_THWOMP] = {0,0,0,0,0,0,0,0};


/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 24;



/*********************************************************/
/* fonction de dessin de la scène à l'écran              */
static void drawFunc(void) { 
	    
/*********Textures************/


	/* reinitialisation des buffers : couleur et ZBuffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(1., 0., 0., 1.);
	glMatrixMode(GL_MODELVIEW);

	/* modification de la matrice de la scène */
	glDisable(GL_LIGHTING);

  
	glLoadIdentity();
	/* Debut du dessin de la scène */
	glPushMatrix();
	eyeX = depth*sin(theta_long_rot_aro_z)*sin(phi_lat_rot_aro_y);
	eyeY = depth*cos(phi_lat_rot_aro_y);
	eyeZ = depth*cos(theta_long_rot_aro_z)*sin(phi_lat_rot_aro_y);
	/* placement de la caméra */
	gluLookAt(eyeX + offX, 
			  eyeY + offY, 
			  eyeZ + offZ,
			  offX, offY, offZ, 
			  0.0, 1.0, 0.0);
	//printf('%f,%f, %f \n',offX, offY, offZ );


	glPushMatrix();
	glRotatef(obj_rot,0.0,1.0,0.0);
	glColor3f(1.0,1.0,1.0);

 		glPushMatrix();
        glDepthMask(GL_FALSE);
		//glTranslatef(offX, offY, offZ);
      	drawBox(100., tabTextureId);
        glDepthMask(GL_TRUE);
        glPopMatrix();

		//canal alpha
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//afficher et centrer le terrain, afficher les trees
		glPushMatrix();
			glTranslatef(-((((float)HeightmapTest->pmginfos->width-1)/(float)HeightmapTest->pmginfos->width)*(float)HeightmapTest->xsize)/2.,0.,-((((float)HeightmapTest->pmginfos->height-1)/(float)HeightmapTest->pmginfos->height)*(float)HeightmapTest->ysize)/2.);
			drawLeaves(HeightmapTest->quadtree, HeightmapTest, tabMapTextures, &modeVisu);

			glPushMatrix();
			for(int i=0; i<scene->nbThwomp; i++){
				glPushMatrix();
				drawTree(scene, treeTexture, i, thwompMouvs[i]);
				glPopMatrix();
			}
			glPopMatrix();
		glPopMatrix();

	//printf(" PLEASE : %d \n", modeVisu);
	//printf(" POS CAMERA : %f %f %f \n", offX, offY, offZ);

    // Réinitialisation des états OpenGL
    //glDisable(GL_TEXTURE_CUBE_MAP_ARB); 

	/* Fin du dessin */
	glPopMatrix();
	/* Fin de la définition de la scène */
	glFinish();
	/* Changement de buffer d'affichage */
	glutSwapBuffers();
}

/*********************************************************/
/* fonction de changement de dimension de la fenetre     */
/* paramètres :                                          */
/* - width : largeur (x) de la zone de visualisation     */
/* - height : hauteur (y) de la zone de visualisation    */
static void reshapeFunc(int width,int height) {
	GLfloat aspect_ratio = (GLfloat) width / (GLfloat) height ;
	/* dimension de l'écran GL */
	glViewport(0, 0, (GLint)width, (GLint)height);
	/* construction de la matrice de projection */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* définition de la camera */
	gluPerspective(scene->fov, aspect_ratio, scene->Znear/100.0, scene->Zfar);	// Angle de vue, rapport largeur/hauteur, near, far
	/* Retour a la pile de matrice Modelview et effacement de celle-ci */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/*********************************************************/
/* fonction associée aux interruptions clavier           */
/* paramètres :                                          */
/* - c : caractère saisi                                 */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdFunc(unsigned char key, int xCoord, int yCoord) {
	/* sortie du programme si utilisation des touches ESC, */
	/* 'q' ou 'Q'*/
	switch(key) {
		case 27  :
			exit(0);
			break;
		case 'Z' : case 'z' : offZ -= 0.05;
			break;
		case 'Q' : case 'q' : offX -= 0.05;
			break;
		case 'S' : case 's' : offZ += 0.05;
			break;
		case 'D' : case 'd' : offX += 0.05;   
			break;
		case 'A' : case 'a' : offY += 0.05;
			break;
		case 'E' : case 'e' : offY -= 0.05;
			break;
		case 'P' : case 'p' : modeVisu = toggleModeVisu(&modeVisu); //pour changer de mode texture à mode quadtree/filaire
			break;
		case 'F' : case 'f' : offY;
			//break;
	}
	glutPostRedisplay();
}

/*********************************************************/
/* fonction associée aux interruptions clavier pour les  */
/*          touches spéciales                            */
/* paramètres :                                          */
/* - c : code de la touche saisie                        */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdSpFunc(int key, int xCoord, int yCoord) {
	/* sortie du programme si utilisation des touches ESC, */
	switch(key) {
		case GLUT_KEY_UP :
			if(phi_lat_rot_aro_y > STEP_ANGLE)
				phi_lat_rot_aro_y += STEP_ANGLE;
			break;
		case GLUT_KEY_DOWN :
			if(phi_lat_rot_aro_y < M_PI - STEP_ANGLE)
				phi_lat_rot_aro_y -= STEP_ANGLE;
			break;
		case GLUT_KEY_LEFT :
			theta_long_rot_aro_z += STEP_ANGLE;
			break;
		case GLUT_KEY_RIGHT :
			theta_long_rot_aro_z -= STEP_ANGLE;
			break;
		// case GLUT_KEY_F1 :
		// 	if (depth>0.1+STEP_PROF) 
		// 		depth -= STEP_PROF*3;
		// 	break;
		// case GLUT_KEY_F2 :
		// 	depth += STEP_PROF*3;
		// 	break;
	}
	glutPostRedisplay();
}

/*********************************************************/
/* fonction d'initialisation des paramètres de rendu et  */
/* des objets de la scènes.                              */
static void init() {

	obj_rot = 0.0;
	size_pt = 5;

//récupération des textures de la skybox
offX = 0;
offY = 0.4;
offZ = 0.8;

	/* INITIALISATION DES PARAMETRES GL */
	/* couleur du fond (gris sombre) */
	glClearColor(0.3,0.3,0.3,0.0);
	/* activation du ZBuffer */
	glEnable( GL_DEPTH_TEST);

	/* lissage des couleurs sur les facettes */
	glShadeModel(GL_SMOOTH);
}

void idle(void) {

	/* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

	/* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS) 
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }

		/* Mise a jour montee et descente des thwomps */
		for(int i=0; i<scene->nbThwomp; i++){
			if(thwompsGetDowntab[i])
			{
				thwompMouvs[i] -= 0.01;
			}
			else
			{
				thwompMouvs[i] += 0.01;
			}
			if(thwompMouvs[i] >= maxThwompMouv)
			{
				thwompsGetDowntab[i] = 1;
			}
			if(thwompMouvs[i] <= minThwompMouv)
			{
				thwompsGetDowntab[i] = 0;
			}
		}

	glutPostRedisplay();
}






int main(int argc, char** argv) {

	/* traitement des paramètres du programme propres à GL */
	glutInit(&argc, argv);

	char* infos_path;

	//passer nom de config en commande
	if(argc == 2){
		const char* prefixe = "./doc/";
		const char* TimacFileName = argv[1];
		const char* extension = ".timac";
		
		infos_path = malloc(strlen(prefixe) + strlen(TimacFileName) + strlen(extension) + 1);

    	strcpy(infos_path, prefixe);
    	strcat(infos_path, TimacFileName);
		strcat(infos_path, extension);

		//printf("INFOS PATH ARGV : %s \n", infos_path);
	}
	else{
		infos_path = "./doc/heightmap-infos.timac";
	}

	HeightmapTest = (Heightmap*) malloc(sizeof(Heightmap));
	
	

	//drawLeaves(heightmap->quadtree);
    HeightmapTest = initializeHeightmap(infos_path, HeightmapTest);

	scene = initializeScene();
	recupinfotimacZnearfar(infos_path,scene);


	/* RVB + ZBuffer + Double buffer.                      */
	glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
	/* placement et dimentions originales de la fenêtre */
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1920, 1080);
	/* ouverture de la fenêtre */
	if (glutCreateWindow("VisuTerrImac") == GL_FALSE) {
		return 1;
	}

	//chargements des images de la skybox
	    char* path[6]={
				"./assets/center_right_skybox.jpg", 
				"./assets/up_skybox.jpg",
				"./assets/skybox_left2.jpg", 
				"./assets/down_skybox.jpg",
				"./assets/center_skybox2.jpg",
				"./assets/right_skybox.jpg"
				};
    for(int i = 0; i<6; i++){
        tabTextureId[i] = getJpgTextureJpg(path[i]);
    }

	//chargements des images pour la texture du terrain
	    char* fileNameMapText[3]={
				"./assets/texture-map-black.jpg",
				"./assets/texture-map-brown.jpg", 
				"./assets/texture-map-green.jpg"
				};
    for(int i = 0; i<3; i++){
        tabMapTextures[i] = getJpgTextureJpg(fileNameMapText[i]);
    }

	treeTexture = getPngTexturePng("./assets/Thwompcarrecenter.png");

	//hauteur de départ des différents thwomps random
	for(int i=0; i<NB_THWOMP; i++){
		thwompMouvs[i] = (float) (rand()%100)/1000;
	}

	init();
	/* couleur de fond */
	glClearColor(0.3,0.3,0.3,0.0);
	/* activation du ZBuffer */
	glEnable( GL_DEPTH_TEST);
	/* association de la fonction callback de redimensionnement */
	glutReshapeFunc(reshapeFunc);
	/* association de la fonction callback d'affichage */
	glutDisplayFunc(drawFunc);
	/* association de la fonction callback d'événement du clavier */
	glutKeyboardFunc(kbdFunc);
	/* association de la fonction callback d'événement du clavier (touches spéciales) */
	glutSpecialFunc(kbdSpFunc);

	glutIdleFunc(idle);

	/* boucle principale de gestion des événements */
	glutMainLoop();
	/* Cette partie du code n'est jamais atteinte */


	//on libere la memoire des textures pour le terrain
	for(int i=0; i<4; i++){
        glDeleteTextures(1, &tabMapTextures[i]);
    }
	for(int i=0; i<7; i++){
        glDeleteTextures(1, &tabTextureId[i]);
    }
	glDeleteTextures(1, &treeTexture);

	//on libere la memoire allouee pour la heightmap, en esperant que ca libere le reste avec
	free(HeightmapTest);
	free(scene);


	return 0;
}