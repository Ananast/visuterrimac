#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "scene.h"
#include "camera.h"
#include "draw.h"
#define TAILLE_MAX 50


Scene* initializeScene(){
    Scene* scene = (Scene*) malloc(sizeof(Scene));

    Heightmap* HeightmapTest = (Heightmap*) malloc(sizeof(Heightmap));
	const char* infos_path = "doc/heightmap-infos.timac";
	
    scene->heightmap = initializeHeightmap(infos_path, HeightmapTest);
    
    srand(7);

    scene->nbThwomp = NB_THWOMP;
    
    for(int i=0; i<scene->nbThwomp; i++){
        int xTree = rand() % (scene->heightmap->pmginfos->width);
        int yTree = rand() % (scene->heightmap->pmginfos->height);
        int zTree = scene->heightmap->pmginfos->pixels[xTree][yTree];
        Point3D* posTree = createPoint3D(xTree, yTree, zTree);

        scene->tree[i] = createTree(0.05, 0.05, posTree);
        //printf(" ARBRE POS INIT SCENE : %f %f %f %f %f \n", scene->tree[i]->posTree->x, scene->tree[i]->posTree->y, scene->tree[i]->posTree->z, scene->tree[i]->width, scene->tree[i]->height);
    }

    return scene;
}


void recupinfotimacZnearfar(const char* infos_path,Scene *scene){

    //donnees du timac
    char file_name[TAILLE_MAX];
    int x_size = 0;
    int y_size = 0;
    float z_min = 0;
    float z_max = 0;
    int Znear = 0;
    int Zfar = 0;
    int fov = 0;

    FILE* timacFile;
    timacFile = fopen(infos_path, "rb");
    if (timacFile == NULL) {
        perror("cannot open file timac to read");
        exit(EXIT_FAILURE);
    }

else{
    fscanf(timacFile, "%s %d %d %f %f %d %d %d", file_name, &x_size, &y_size, &z_min, &z_max, &Znear, &Zfar, &fov);
    //printf("info config : %s %d %d %f %f %d %d %d\n", file_name, x_size, y_size, z_min, z_max, Znear, Zfar, fov);

    fclose(timacFile);

    scene->Zfar = Zfar;
    scene->Znear = Znear;
    scene->fov = fov;
    }
}

Tree* createTree(float widthTree, float heightTree, Point3D* position){
    Tree* newTree = (Tree*) malloc(sizeof(Tree));
    newTree->width = widthTree;
    newTree->height = heightTree;
    newTree->posTree = position;

    return newTree;
}

void drawTree(Scene* scene, GLuint* treeTexture, int i, float thwompMouv){
    //int i sert à compter les thwomps

        float widthPgm = (float)scene->heightmap->pmginfos->width;
        float xsize = (float)scene->heightmap->xsize;

        float heightPgm = (float)scene->heightmap->pmginfos->height;
        float ysize = (float)scene->heightmap->ysize;

        float hauteurPgm = (float)scene->heightmap->pmginfos->max_gray;
        float zmin = (float)scene->heightmap->zmin;
        float zmax = (float)scene->heightmap->zmax-(float)scene->heightmap->zmin;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, treeTexture);
        
        glPushMatrix();
        glTranslatef(((float)scene->tree[i]->posTree->x/widthPgm)*xsize,zmin+((float)scene->tree[i]->posTree->z/hauteurPgm)*zmax+thwompMouv,((float)scene->tree[i]->posTree->y/heightPgm)*ysize);
        glPushMatrix();
        glRotatef(theta_long_rot_aro_z*(360/(2*M_PI)), 0.0, 1.0, 0.0);
        glTranslatef(-scene->tree[i]->width/2.,0.,0.);


        /*glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
        glVertex3f(((float)scene->tree[i]->posTree->x/widthPgm)*xsize + scene->tree[i]->width, zmin+((float)scene->tree[i]->posTree->z/hauteurPgm)*zmax + scene->tree[i]->height, ((float)scene->tree[i]->posTree->y/heightPgm)*ysize);

		glTexCoord2f(1, 0);
        glVertex3f(((float)scene->tree[i]->posTree->x/widthPgm)*xsize, zmin+((float)scene->tree[i]->posTree->z/hauteurPgm)*zmax + scene->tree[i]->height, ((float)scene->tree[i]->posTree->y/heightPgm)*ysize);

		glTexCoord2f(1, 1);
        glVertex3f(((float)scene->tree[i]->posTree->x/widthPgm)*xsize, zmin+((float)scene->tree[i]->posTree->z/hauteurPgm)*zmax, ((float)scene->tree[i]->posTree->y/heightPgm)*ysize);

		glTexCoord2f(0, 1);
        glVertex3f(((float)scene->tree[i]->posTree->x/widthPgm)*xsize + scene->tree[i]->width, zmin+((float)scene->tree[i]->posTree->z/hauteurPgm)*zmax, ((float)scene->tree[i]->posTree->y/heightPgm)*ysize);
	    glEnd();*/
        glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
        glVertex3f(scene->tree[i]->width, scene->tree[i]->height, 0);

		glTexCoord2f(1, 0);
        glVertex3f(0, scene->tree[i]->height, 0);

		glTexCoord2f(1, 1);
        glVertex3f(0, 0, 0);

		glTexCoord2f(0, 1);
        glVertex3f(scene->tree[i]->width, 0, 0);
	    glEnd();

        glPopMatrix();

        glPopMatrix();
        

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);   
    glEnd();
}