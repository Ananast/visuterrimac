#include <stdlib.h>
#include <stdio.h>

#include <math.h>

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "camera.h"

/* déclaration et définition des variables globales pour la gestion de la caméra */
float depth = 0.01;
float phi_lat_rot_aro_y = M_PI/2.0;
float theta_long_rot_aro_z = 0.0;

double eyeX;
double eyeY;
double eyeZ;

float offX = 0;
float offY = 0;
float offZ = 0;
