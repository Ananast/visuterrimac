#include <stdlib.h>
#include <stdio.h>

#include <math.h>

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "objects.h"

void glDrawOrigin(float axisLength) 
{
    glBegin(GL_LINES);
        glColor3f(1., 0., 0.);
        glVertex3f(0., 0., 0.);
        glVertex3f(axisLength, 0., 0.);

        glColor3f(0., 1., 0.);
        glVertex3f(0., 0., 0.);
        glVertex3f(0., axisLength, 0.);

        glColor3f(0., 0., 1.);
        glVertex3f(0., 0., 0.);
        glVertex3f(0., 0., axisLength);
    glEnd();
}