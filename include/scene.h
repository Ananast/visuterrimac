#ifndef SCENE_H
#define SCENE_H

#include "heightmap.h"
#include "geometry.h"
#include "draw.h"
#include <stdio.h>

#define NB_THWOMP 8

typedef struct Tree {
    float width;
    float height;
    Point3D* posTree;
} Tree;

typedef struct Scene {
    Heightmap* heightmap;
    int Znear;
    int Zfar;
    int fov;

    Tree* tree[20];
    int nbThwomp;
} Scene;

Scene* initializeScene();
void recupinfotimacZnearfar(const char* infos_path,Scene *scene);

Tree* createTree(float widthTree, float heightTree, Point3D* position);
void drawTree(Scene* scene, GLuint* treeTexture, int countTree, float thwompMouv);


#endif