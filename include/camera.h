/* déclaration et définition des constantes globales pour la gestion de la caméra */
#define M_PI        3.14159265358979323846
#define STEP_ANGLE	M_PI/90.
#define STEP_PROF	M_PI/90.
 
/* déclaration des variables globales pour la gestion de la caméra */
extern float depth;
extern float phi_lat_rot_aro_y;
extern float theta_long_rot_aro_z;
extern double eyeX;
extern double eyeY;
extern double eyeZ;
extern float offX;
extern float offY;
extern float offZ;

/* déclaration des des structure */


/* déclaration des fonctions */

