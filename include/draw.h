#ifndef DRAW_H
#define DRAW_H
#include "geometry.h"

//linux
#include <GL/gl.h>
#include <GL/glu.h>



//Dessine le cube de la Skybox
void drawBox(float length , GLuint* textureSky);
//récupérer les textures de la skybox
GLuint getJpgTextureJpg(char* path);
GLuint getPngTexturePng(char* path);
//changer le mode de visualisation entre texture et mode filaire
int toggleModeVisu(int* modeVisu);




#endif