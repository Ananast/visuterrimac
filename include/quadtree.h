#ifndef QUADTREE_H
#define QUADTREE_H

#include "heightmap.h"
#include "geometry.h"


typedef struct Leaf {
    Point3D* pointHG;
    Point3D* pointHD;
    Point3D* pointBG;
    Point3D* pointBD;
} Leaf;

typedef struct Node {
    struct Node* HautGauche;
    struct Node* HautDroite;
    struct Node* BasGauche;
    struct Node* BasDroite;
    
    Point3D* limitHG;
    Point3D* limitBD;

    Leaf* leaf;
} Node;

Node* InitializeQuadtree(struct Heightmap* heightmap);
Point3D* createPoint3D(int x, int y, int z);
void insertNodes(Node* node, struct Heightmap* heightmap);
Leaf* createLeaf(Point3D* limitHG, Point3D* limitBD, struct Heightmap* heightmap);
void drawLeaves(Node* node, struct Heightmap* heightmap, GLuint* tabTextureMap, int* modeVisu);
void drawLeavesLineMode(Node* node, float widthPgm, float xsize, float heightPgm, float ysize, float hauteurPgm, float zmin, float zmax, float moyennehauteur);
void drawLeavesTexMode(Node* node, GLuint* tabTextureMap, float widthPgm, float xsize, float heightPgm, float ysize, float hauteurPgm, float zmin, float zmax, float moyennehauteur, int chooseTexture);
Leaf* searchLeaf(Node* node, int x, int y);
int frustumCullingPossible(Node* node, struct Heightmap* heightmap, double eyeX, double eyeY, double eyeZ, int fov);

#endif