#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "quadtree.h"
#include <stdio.h>

typedef struct PGMData {
    int width;
    int height;
    int max_gray;
    int **pixels;
} PGMData;

typedef struct Heightmap {
    PGMData* pmginfos;
    struct Node* quadtree;
    int xsize;
    int ysize;
    float zmin;
    float zmax;
} Heightmap;

void loadPGM(const char* file_path, PGMData* data);
void SkipComments(FILE *fp);

Heightmap* initializeHeightmap(const char* infos_path, Heightmap* Heightmap);

#endif