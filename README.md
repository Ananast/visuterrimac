# VisuTerrIMAC

Projet de prog/algo & synthèse d'image

### Compilation

**make clean** pour nettoyer le dossier.

**make** pour tout compiler.

**bin/visuterrimac.out** pour exécuter

### Commandes

**a** pour monter ; **e** pour descendre

**z** pour avancer ; **s** pour reculer

**q** pour aller vers la gauche ; **d** pour aller vers la droite

**flèches directionnelles** pour faire une rotation de la caméra

### Autres

Ce projet nécessite SDL2_image pour fonctionner.
